package by.bsuir.library.model.subscription;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import by.bsuir.library.model.book.Book;
import by.bsuir.library.model.user.User;

public class Subscription implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7004677943593351237L;

	int id;
	int userId;
	List<Book> books;
	SubscrType subscription;
	User user;
	Date openDate;

	public Subscription(int userId, SubscrType subscription, Date openDate,
			List<Book> books) {
		super();
		this.userId = userId;
		this.books = new ArrayList<Book>();
		if (books != null) {
			this.books.addAll(books);
		}
		this.subscription = subscription;
		this.openDate = openDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public SubscrType getSubscription() {
		return subscription;
	}

	public void setSubscription(SubscrType subscription) {
		this.subscription = subscription;
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}


	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean containsZeroBooks() {
		for (Book book : books) {
			if (book.getQuantity() == 0) {
				return true;
			}
		}
		return false;
	}

	public List<Book> getCommonBooks(List<Book> books) {
		List<Book> common = new ArrayList<Book>();
		for (Book book : books) {
			if (commonBook(book)) {
				common.add(book);
			}
		}
		return common;
	}

	private boolean commonBook(Book cBook) {
		for (Book book : this.books) {
			if (book.equals(cBook))
				return true;
		}
		return false;
	}
}
