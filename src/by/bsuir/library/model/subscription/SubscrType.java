package by.bsuir.library.model.subscription;

public enum SubscrType {

	HOME(1), ZALA(2);
	int intVal;

	private SubscrType(int intVal) {
		this.intVal = intVal;
	}

	public int value() {
		return intVal;
	}

}
