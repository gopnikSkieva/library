package by.bsuir.library.model.subscription;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import by.bsuir.library.model.book.Book;

public class SubscriptionList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1906715410852791394L;

	List<Subscription> subs = new ArrayList<Subscription>();

	public List<Subscription> getSubs() {
		return subs;
	}

	public void setSubs(List<Subscription> subs) {
		this.subs = subs;
	}
	
	public void removeAll(Collection<?> T){
		subs.removeAll(T);
	}

	public List<Book> getCommonBooks(List<Book> books) {
		List<Book> common = new ArrayList<Book>();
		for(Subscription sub:subs){
			common.addAll(sub.getCommonBooks(books));
		}
		return common;
	}

}
