package by.bsuir.library.model.book;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import by.bsuir.library.dao.book.BookDAO;
import by.bsuir.library.dao.subscription.SubscriptionDAO;
import by.bsuir.library.dao.subscription.SubscriptionRequestDAO;
import by.bsuir.library.model.subscription.SubscriptionList;
import by.bsuir.library.model.user.User;

public class BookTable implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3208596159801762424L;
	private List<Book> books;
	private ListIterator<Book> it;
	
	
	public BookTable() {
		books = new ArrayList<Book>();
		it = books.listIterator();
	}

	public List<Book> getBooks() {
		return books;
	}
	
	public List<Integer> getBookIds() {
		List<Integer> res = new ArrayList<Integer>();
		for (Book book:books){
			res.add(book.getId());
		}
		return res;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
		it = books.listIterator();
	}

	/**
	 * Gets an element of the rowArray property
	 */
	public Book getBook(int index) {
		return this.books.get(index);
	}

	/**
	 * Sets an element of the rowArray property
	 */
	public void setBook(int index, Book book) {
		this.books.set(index, book);
	}

	/**
	 * Returns the number of selected rows
	 */
	public int countSelectedRows() {
		int count = 0;
		for (int i = 0; i < books.size(); i++)
			if (books.get(i).isSelected())
				count++;
		return count;
	}

	/**
	 * Appends a new row at the end of the table
	 */
	public void appendNewRow(Book book) {
		if (!hasBook(book)) {
			books.add(book);
		}
	}

	/**
	 * Deletes the selected rows
	 */
	public void deleteSelectedRows() {
		for (int i = 0; i < books.size(); i++) {
			if (books.get(i).isSelected())
				books.remove(i);
		}
	}

	public boolean hasBook(Book b) {
		for (Book book : books) {
			if (book.equals(b))
				return true;
		}
		return false;
	}

	public boolean removeBookById(int id) {
		for (Book book : books) {
			if (book.getId() == id)
				return books.remove(book);
		}
		return false;
	}

	public Book getNext() {
		Book b = null;
		if (it.hasNext())
			b = it.next();
		return b;
	}

	public Book getCurr() {
		Book b = null;
		if (it.hasNext()) {
			it.next();
			b = it.previous();
		}
		return b;
	}

	public int getSize() {
		return books.size();
	}

	public boolean containsZeroBooks() {
		for (Book book:books){
			if (book.getQuantity()==0){
				return true;
			}
		}
		return false;
	}

	public List<Book> requestedUserBooks(User user) {
		SubscriptionList slist;
		List<Book> common = new ArrayList<Book>();
		SubscriptionDAO sdao = new SubscriptionDAO();
		slist = sdao.findAllByUser(user);
		common.addAll(slist.getCommonBooks(books));
		SubscriptionRequestDAO srdao = new SubscriptionRequestDAO();
		slist = srdao.findAllByUser(user);
		common.addAll(slist.getCommonBooks(books));	
		return common;
	}

	public void dec() {
		BookDAO bdao = new BookDAO();
		for (Book book:books){
			bdao.dec(book.getId());
		}
	}
	
	public void inc() {
		BookDAO bdao = new BookDAO();
		for (Book book:books){
			bdao.inc(book.getId());
		}
	}
}
