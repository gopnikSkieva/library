package by.bsuir.library.model.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserList {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1906715410852791394L;

	List<User> users = new ArrayList<User>();

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public void removeAll(Collection<?> T) {
		users.removeAll(T);
	}
	
	public void addAll(Collection<? extends User> T) {
		users.addAll(T);
	}

	public User getById(int id) {
		for (User user : users) {
			if (user.getId() == id) {
				return user;
			}
		}
		return null;
	}
}
