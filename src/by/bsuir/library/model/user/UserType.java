package by.bsuir.library.model.user;

public enum UserType {

	USER, ADMIN, GUEST
}
