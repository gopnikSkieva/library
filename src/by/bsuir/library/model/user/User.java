package by.bsuir.library.model.user;

public class User {
	
	private String name;
	private UserType type;
	private boolean logged;
	private String password;
	private int id;

	public User(String name) {
		super();
		this.name = name;
		password = "guest";
		setLogged(false);
		setType(UserType.GUEST);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}

	public boolean isLogged() {
		return logged;
	}

	public void setLogged(boolean logged) {
		this.logged = logged;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
