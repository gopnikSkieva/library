package by.bsuir.library.event;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

import by.bsuir.library.model.user.User;

/**
 * Application Lifecycle Listener implementation class UserLoggingEvent
 * 
 */
public class UserLoggingEvent implements HttpSessionListener,
		HttpSessionAttributeListener {
	private static Logger log = Logger.getLogger(UserLoggingEvent.class);

	/**
	 * Default constructor.
	 */
	public UserLoggingEvent() {
	}

	/**
	 * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
	 */
	public void sessionCreated(HttpSessionEvent hse) {
		System.out.println("CREATE: " + hse);
		log.info("session CREATE: " + hse);
	}

	/**
	 * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
	 */
	public void sessionDestroyed(HttpSessionEvent hse) {
		HttpSession session = hse.getSession();
		long start = session.getCreationTime();
		long end = session.getLastAccessedTime();
		String counter = (String) session.getAttribute("counter");
		System.out.println("DESTROY, Session Duration:" + (end - start)
				+ "(ms) Counter:" + counter + hse);
		log.info("session DESTROY, Session Duration:" + (end - start)
				+ "(ms) Counter:" + counter + hse);
	}

	@Override
	public void attributeAdded(HttpSessionBindingEvent hsbe) {
		if ("user".equals(hsbe.getName())
				&& ((User) hsbe.getValue()).isLogged()) {
			System.out.println(((User) hsbe.getValue()).getName()
					+ " logged in");
			log.info(((User) hsbe.getValue()).getName()
						+ " logged in");
		}

	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent hsbe) {
		// TODO Auto-generated method stub

	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent hsbe) {
		if ("user".equals(hsbe.getName())) {
			if ("guest".equals(((User) hsbe.getValue()).getName())) {
				System.out.println(((User) hsbe.getSession().getAttribute(
						"user")).getName()
						+ " logged in");
				log.info(((User) hsbe.getValue()).getName()
						+ " logged in");
			} else {
				System.out.println(((User) hsbe.getValue()).getName()
						+ " logged out");
				log.info(((User) hsbe.getValue()).getName()
						+ " logged out");
			}
		}

	}
}
