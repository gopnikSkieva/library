package by.bsuir.library.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

public class PageLinks extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 966277425690557963L;
	private static Logger log = Logger.getLogger(PageLinks.class);

	int page;

	public int doStartTag() throws JspException {
		try {
			pageContext.getOut().println("<div id=\"pagelinks\">");
			pageContext
					.getOut()
					.print(" [<a href=\"library?command=viewbooksswitchpage&pageLink=FIRST\">first</a>] ");
			pageContext
					.getOut()
					.print(" [<a href=\"library?command=viewbooksswitchpage&pageLink=PREV\">prev</a>] ");
			pageContext
					.getOut()
					.print(" [<a href=\"library?command=viewbooksswitchpage&pageLink=NEXT\">next</a>] ");
			pageContext
					.getOut()
					.print(" [<a href=\"library?command=viewbooksswitchpage&pageLink=LAST\">last</a>] ");
			pageContext
					.getOut()
					.println(
							"<select onchange=\"if(this.options.selectedIndex>0) location = this.options[this.selectedIndex].value;\">");
			pageContext
			.getOut()
			.println(
					"<option value=\"display\">Display by:</option>");
			pageContext
			.getOut()
			.println(
					"<option value=\"library?command=viewbooksswitchpage&pageSize=5\">5</option>");
			pageContext
					.getOut()
					.println(
							"<option value=\"library?command=viewbooksswitchpage&pageSize=10\">10</option>");
			pageContext
					.getOut()
					.println(
							"<option value=\"library?command=viewbooksswitchpage&pageSize=20\">20</option>");
			pageContext
					.getOut()
					.println(
							"<option value=\"library?command=viewbooksswitchpage&pageSize=50\">50</option>");
			pageContext
					.getOut()
					.println(
							"<option value=\"library?command=viewbooksswitchpage&pageSize=100\">100</option>");
			pageContext.getOut().print("</select>");
			pageContext.getOut().println("</div>");

		} catch (IOException e) {
			log.error("IOException during parsing tag" + e);
		}
		return SKIP_BODY;
	}

	public void setPage(int page) {
		this.page = page;
	}

}