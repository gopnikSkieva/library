package by.bsuir.library.tag;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import by.bsuir.library.model.book.BookTable;

public class PagedTable extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 966277425690557963L;
	private static final String ATTR_NAME_MAX_LINES = "maxLines";
	private static Logger log = Logger.getLogger(PagedTable.class);

	BookTable bookTable;
	int maxLines;
	int page;
	boolean editable;

	private ResourceBundle resourceBundle;

	public int doStartTag() throws JspException {
		try {
			if (maxLines <= 0) {
				maxLines = 10;
				pageContext.getSession().setAttribute(ATTR_NAME_MAX_LINES, 10);
			}
			if (page <= 0) {
				page = 1;
			}
			if (bookTable != null && bookTable.getBooks().size() > 0) {
				if (page <= (bookTable.getBooks().size() / maxLines) + 1) {
					drawTable();
				} else {
					throw new Exception("requested page does not exist");
				}

			}

		} catch (IOException e) {
			log.error("IOException during parsing tag " + e);
		} catch (Exception e) {
			log.error("Exception during parsing tag " + e);
		}
		return SKIP_BODY;
	}

	public void setBookTable(BookTable bookTable) {
		this.bookTable = bookTable;
	}

	public void setMaxLines(int maxLines) {
		this.maxLines = maxLines;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public void setEditable(boolean isEditable) {
		this.editable = isEditable;
	}

	private void drawTable() throws IOException {
		pageContext
				.getOut()
				.println(
						"<table border=\"2\" cellpadding=\"2\" cellspacing=\"2\" width=\"400px\">");
		drawHeader();
		drawContents();
		pageContext.getOut().println("</table>");
	}

	private void drawContents() throws IOException {
		int displayContentMin = (page - 1) * maxLines;
		int displayContentMax = ((displayContentMin + maxLines) < bookTable
				.getBooks().size()) ? (displayContentMin + maxLines)
				: bookTable.getBooks().size();
		if (displayContentMin > displayContentMax) {
			throw new IOException("wrong attribute values in tag PagedTable");
		}
		for (int i = displayContentMin; i < (displayContentMax); i++) {
			pageContext.getOut().println("<tr>");
			pageContext.getOut().println(
					"<td>" + bookTable.getBooks().get(i).getId() + "</td>");
			pageContext.getOut().println(
					"<td>" + bookTable.getBooks().get(i).getName() + "</td>");
			pageContext.getOut().println(
					"<td>" + bookTable.getBooks().get(i).getAuthor() + "</td>");
			pageContext.getOut().println(
					"<td>" + bookTable.getBooks().get(i).getQuantity()
							+ "</td>");
			if (editable) {
				pageContext
						.getOut()
						.println(
								"<td><img src=\"images/edit_book.png\" height=\"16\" width=\"16\" onclick=\"javascript:updatebook("
										+ bookTable.getBooks().get(i).getId()
										+ ")\"></td>");
				pageContext
						.getOut()
						.println(
								"<td><img src=\"images/rem_book.png\" height=\"16\" width=\"16\" onclick=\"javascript:deletebook("
										+ bookTable.getBooks().get(i).getId()
										+ ")\"></td>");
			} else {
				pageContext.getOut().println(
						"<td><input type=\"checkbox\" name=\"cb\" value="
								+ bookTable.getBooks().get(i).getId()
								+ "></td>");
			}
			pageContext.getOut().println("</tr>");
		}

	}

	private void drawHeader() throws IOException {

		String locale = ((Locale) pageContext.getSession().getAttribute(
				"userLocale")).getLanguage();
		if (null != locale) {
			resourceBundle = ResourceBundle.getBundle("properties/text_"
					+ locale);
		} else {
			resourceBundle = ResourceBundle.getBundle("properties/text");
		}
		pageContext.getOut().println("<tr>");
		pageContext.getOut().println("<th>" + getProperty("bookid") + "</th>");
		pageContext.getOut().println("<th>" + getProperty("name") + "</th>");
		pageContext.getOut().println("<th>" + getProperty("author") + "</th>");
		pageContext.getOut()
				.println("<th>" + getProperty("quantity") + "</th>");
		pageContext.getOut().println("</tr>");
	}

	private String getProperty(String key) {
		return (String) resourceBundle.getObject(key);
	}
}
