package by.bsuir.library.connectionpool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

public class ConnectionPool {
	public static final String PROPERTIES_FILE = "properties/pool";
	public static final int DEFAULT_POOL_SIZE = 10;
	private static Logger log = Logger.getLogger(ConnectionPool.class);
	private static ConnectionPool instance = null;

	private BlockingQueue<Connection> connectionQueue;

	private ConnectionPool(String url, int poolSize)
			throws SQLException {

		connectionQueue = new ArrayBlockingQueue<Connection>(poolSize);

		for (int i = 0; i < poolSize; i++) {
			Connection connection = DriverManager.getConnection(url);
			connectionQueue.offer(connection);
		}
	}

	public static void init() {
		if (instance == null) {
			ResourceBundle rb = ResourceBundle.getBundle(PROPERTIES_FILE);

			String url = rb.getString("db.url");
			String poolSizeStr = rb.getString("db.poolsize");
			int poolSize = (poolSizeStr != null) ? Integer
					.parseInt(poolSizeStr) : DEFAULT_POOL_SIZE;

			try {
				log.debug("Trying to create pool of connections...");
				instance = new ConnectionPool(url, poolSize);
				log.debug("Connection pool succesfully initialized");
			} catch (SQLException e) {
				log.fatal("Cannot establish connection with database");
				throw new RuntimeException(e);
			}
		}
	}

	public static void dispose() throws SQLException {
		if (instance != null) {
			instance.clearConnectionQueue();
			instance = null;
			log.debug("Connection pool succesfully disposed");
		}
	}

	public static ConnectionPool getInstance() {
		return instance;
	}

	public Connection getConnection() {
		Connection connection = null;
		try {
			connection = connectionQueue.take();
		} catch (InterruptedException e) {
			log.warn(
					"Free connection waiting interrupted. Returned `null` connection",
					e);
		}
		return connection;
	}

	public void closeConnection(Connection connection) {
		try {
			if (!connection.isClosed()) {
				if (!connectionQueue.offer(connection)) {
					log.warn("Connection not added. Possible `leakage` of connections");
				}
			} else {
				log.warn("Trying to release closed connection. Possible `leakage` of connections");
			}
		} catch (SQLException e) {
			log.warn(
					"SQLException at conection isClosed () checking. Connection not added",
					e);
		}
	}

	private void clearConnectionQueue() throws SQLException {
		Connection connection;
		while ((connection = connectionQueue.poll()) != null) {
			/* see java.sql.Connection#close () javadoc */
			if (!connection.getAutoCommit()) {
				connection.commit();
			}
			connection.close();
		}
	}
}
