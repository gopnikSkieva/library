package by.bsuir.library.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public interface ICommand {

	public String execute(HttpServletRequest request);

	static Logger log = Logger.getLogger(ICommand.class);

}
