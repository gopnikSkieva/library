package by.bsuir.library.command.book;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.book.BookDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.book.Book;
import by.bsuir.library.model.book.BookTable;
import by.bsuir.library.model.user.User;

public class CreateBook implements ICommand {

	private static final String ATTR_NAME_BOOKS = "books";
	private static final String ATTR_NAME_ACTION = "bookaction";
	private static final String ATTR_NAME_EDIT_BOOK = "varbook";
	private static final String PARAM_NAME_NAME = "name";
	private static final String PARAM_NAME_AUTHOR = "author";
	private static final String PARAM_NAME_QUANTITY = "quantity";

	@Override
	public String execute(HttpServletRequest request) {
		BookDAO bookdao = new BookDAO();
		Book book = new Book(0, (String) request.getParameter(PARAM_NAME_NAME),
				(String) request.getParameter(PARAM_NAME_AUTHOR),
				Integer.parseInt(request.getParameter(PARAM_NAME_QUANTITY)));
		bookdao.insert(book);
		request.getSession().setAttribute(ATTR_NAME_ACTION, null);
		request.getSession().setAttribute(ATTR_NAME_EDIT_BOOK, null);
		BookTable books = new BookTable();
		books.setBooks(bookdao.findAll());
		request.getSession().setAttribute(ATTR_NAME_BOOKS, books);
		log.info("new book(" + book.getName() + ") added by "
				+ ((User) request.getSession().getAttribute("user")).getName());
		String page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.BOOK_PAGE_PATH);

		return page;
	}

}
