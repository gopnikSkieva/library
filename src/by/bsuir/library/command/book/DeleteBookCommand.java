package by.bsuir.library.command.book;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.book.BookDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.book.Book;
import by.bsuir.library.model.book.BookTable;
import by.bsuir.library.model.user.User;

public class DeleteBookCommand implements ICommand {

	private static final String ATTR_NAME_BOOKS = "books";
	private static final String ATTR_NAME_ACTION = "bookaction";
	private static final String ATTR_NAME_EDIT_BOOK = "varbook";
	private static final String ATTR_NAME_ID = "numparam";

	@Override
	public String execute(HttpServletRequest request) {
		BookDAO bookdao = new BookDAO();
		int val = (Integer) request.getAttribute(ATTR_NAME_ID);
		Book book = bookdao.findByPrimaryKey(val);
		bookdao.delete(val);
		request.getSession().setAttribute(ATTR_NAME_ACTION, null);
		request.getSession().setAttribute(ATTR_NAME_EDIT_BOOK, null);
		BookTable books = new BookTable();
		books.setBooks(bookdao.findAll());
		request.getSession().setAttribute(ATTR_NAME_BOOKS, books);
		log.info("book(" + book.getName() + ") deleted by "
				+ ((User) request.getSession().getAttribute("user")).getName());
		String page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.BOOK_PAGE_PATH);

		return page;
	}
}
