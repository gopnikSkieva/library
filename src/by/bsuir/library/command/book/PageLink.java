package by.bsuir.library.command.book;

public enum PageLink {

	FIRST, PREV, NEXT, LAST, CURRENT
}
