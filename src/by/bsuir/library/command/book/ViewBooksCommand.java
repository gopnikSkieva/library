package by.bsuir.library.command.book;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.book.BookDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.book.BookTable;

public class ViewBooksCommand implements ICommand {

	private static final String ATTR_NAME_BOOKS = "books";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		BookTable books = new BookTable();
		BookDAO bookdao = new BookDAO();
		books.setBooks(bookdao.findAll());
		request.getSession().setAttribute(ATTR_NAME_BOOKS, books);
		page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.BOOK_PAGE_PATH);

		return page;
	}

}
