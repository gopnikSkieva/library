package by.bsuir.library.command.book;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.book.BookDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.book.Book;

public class UpdateBookViewCommand implements ICommand {

	private static final String ATTR_NAME_ACTION = "bookaction";
	private static final String ATTR_NAME_EDIT_BOOK = "varbook";
	private static final String ATTR_NAME_ID = "numparam";

	@Override
	public String execute(HttpServletRequest request) {
		BookDAO bookdao = new BookDAO();
		int val = (Integer) request.getAttribute(ATTR_NAME_ID);
		Book book = bookdao.findByPrimaryKey(val);
		request.getSession().setAttribute(ATTR_NAME_ACTION, "updat");
		request.getSession().setAttribute(ATTR_NAME_EDIT_BOOK, book);
		String page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.EDIT_BOOK_PAGE_PATH);

		return page;
	}

}
