package by.bsuir.library.command.book;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.book.BookDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.book.BookTable;

public class SearchBook implements ICommand {

	private static final String PARAM_NAME_NAME = "name";
	private static final String PARAM_NAME_AUTHOR = "author";
	private static final String ATTR_NAME_BOOKS = "books";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		BookTable books = new BookTable();
		BookDAO bookdao = new BookDAO();

		String name = (String) request.getParameter(PARAM_NAME_NAME);
		String author = (String) request.getParameter(PARAM_NAME_AUTHOR);
		if (name.length() != 0) {
			if (author.length() != 0) {
				books.setBooks(bookdao.findByNameAndAuthor(name, author));
			} else {
				books.setBooks(bookdao.findByName(name, true));
			}
		} else if (author.length() != 0) {
			books.setBooks(bookdao.findByAuthor(author, true));
		} else {
			books = null;
		}
		request.getSession().setAttribute(ATTR_NAME_BOOKS, books);
		page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.BOOK_PAGE_PATH);

		return page;
	}

}
