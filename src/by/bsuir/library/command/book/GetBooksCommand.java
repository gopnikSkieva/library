package by.bsuir.library.command.book;

import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.book.BookDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.book.BookTable;

public class GetBooksCommand implements ICommand {

	private static final String ATTR_NAME_REQ_BOOKS_LIST = "reqbooks";
	private static final String PARAM_NAME_CHECK_BOX = "cb";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		BookTable books = new BookTable();
		BookDAO bookdao = new BookDAO();
		books.setBooks(bookdao.findAll());
		BookTable resBookTable;
		resBookTable = (BookTable) request.getSession().getAttribute(
				ATTR_NAME_REQ_BOOKS_LIST);
		if (resBookTable == null) {
			resBookTable = new BookTable();
		}
		HashSet<Integer> selected = new HashSet<Integer>();
		String select[] = request.getParameterValues(PARAM_NAME_CHECK_BOX);
		if (select != null && select.length != 0) {
			for (int i = 0; i < select.length; i++) {
				selected.add(Integer.parseInt(select[i]));
			}
		}

		for (int i = 0; i < books.getBooks().size(); i++) {
			if (selected.contains(books.getBook(i).getId())) {
				resBookTable.appendNewRow(books.getBook(i));
			}
		}
		request.getSession().setAttribute(ATTR_NAME_REQ_BOOKS_LIST,
				resBookTable);
		page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.SUBSCR_PAGE_PATH);

		return page;
	}

}
