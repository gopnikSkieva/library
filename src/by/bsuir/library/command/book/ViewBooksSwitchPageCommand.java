package by.bsuir.library.command.book;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.book.BookTable;

public class ViewBooksSwitchPageCommand implements ICommand {

	private static final String ATTR_NAME_BOOKS = "books";
	private static final String ATTR_NAME_PAGE_SIZE = "pageSize";
	private static final String ATTR_NAME_PAGE_NUM = "pageNum";
	private static final String PARAM_NAME_PAGE_LINK = "pageLink";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		Integer pageSize = null;
		Integer pageNum;
		PageLink pageLink = null;
		BookTable books = (BookTable) request.getSession().getAttribute(
				ATTR_NAME_BOOKS);
		if (books.getBooks().size() > 0) {
			String pl = request.getParameter(PARAM_NAME_PAGE_LINK);
			if (null != pl) {
				pageLink = PageLink.valueOf(pl);
			}
			pageSize = (Integer) request.getSession().getAttribute(
					ATTR_NAME_PAGE_SIZE);
			String pageSizeStr = request.getParameter(ATTR_NAME_PAGE_SIZE);
			if (null != pageSizeStr) {
				pageSize = Integer.parseInt(pageSizeStr);
			}
			pageNum = (Integer) request.getSession().getAttribute(
					ATTR_NAME_PAGE_NUM);
			if (null == pageNum) {
				pageNum = 1;
			}
			if (null == pageSize) {
				pageSize = 10;
			}
			pageNum = nextPage(pageNum, pageSize, books.getBooks().size(),
					pageLink);
			request.getSession().setAttribute(ATTR_NAME_PAGE_NUM, pageNum);
			request.getSession().setAttribute(ATTR_NAME_PAGE_SIZE, pageSize);
		}
		page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.BOOK_PAGE_PATH);

		return page;
	}

	private int nextPage(int prevPage, int pageSize, int size, PageLink pageLink) {
		if (null == pageLink) {
			pageLink = PageLink.CURRENT;
		}
		switch (pageLink) {
		case FIRST:
			return 1;
		case PREV:
			return (prevPage > 1) ? (prevPage - 1) : prevPage;
		case NEXT:
			return ((size / pageSize + 1) < (prevPage + 1)) ? (size / pageSize + 1)
					: (prevPage + 1);
		case LAST:
			return size / pageSize + 1;
		case CURRENT:
			return ((prevPage - 1) * pageSize > size) ? 1 : prevPage;
		default:
			throw new IllegalArgumentException("no such type of link");
		}
	}
}
