package by.bsuir.library.command.book;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.manager.ConfigurationManager;

public class CreateBookView implements ICommand {

	private static final String ATTR_NAME_ACTION = "bookaction";
	private static final String ATTR_NAME_EDIT_BOOK = "varbook";

	@Override
	public String execute(HttpServletRequest request) {
		request.getSession().setAttribute(ATTR_NAME_ACTION, "add");
		request.getSession().setAttribute(ATTR_NAME_EDIT_BOOK, null);
		String page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.EDIT_BOOK_PAGE_PATH);
		return page;
	}

}
