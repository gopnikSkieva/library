package by.bsuir.library.command.book;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.book.BookDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.book.Book;
import by.bsuir.library.model.book.BookTable;
import by.bsuir.library.model.user.User;

public class UpdateBook implements ICommand {

	private static final String ATTR_NAME_BOOKS = "books";
	private static final String ATTR_NAME_ACTION = "bookaction";
	private static final String ATTR_NAME_EDIT_BOOK = "varbook";
	private static final String PARAM_NAME_NAME = "name";
	private static final String PARAM_NAME_AUTHOR = "author";
	private static final String PARAM_NAME_QUANTITY = "quantity";
	private static final String ATTR_NAME_USER = "user";
	private static final String ATTR_NAME_ID = "numparam";

	@Override
	public String execute(HttpServletRequest request) {
		BookDAO bookdao = new BookDAO();
		int val = (Integer) request.getAttribute(ATTR_NAME_ID);
		Book book = bookdao.findByPrimaryKey(val);
		book.setName((String) request.getParameter(PARAM_NAME_NAME));
		book.setAuthor((String) request.getParameter(PARAM_NAME_AUTHOR));
		book.setQuantity(Integer.parseInt(request
				.getParameter(PARAM_NAME_QUANTITY)));
		bookdao.update(val, book);
		request.getSession().setAttribute(ATTR_NAME_ACTION, null);
		request.getSession().setAttribute(ATTR_NAME_EDIT_BOOK, null);
		BookTable books = new BookTable();
		books.setBooks(bookdao.findAll());
		request.getSession().setAttribute(ATTR_NAME_BOOKS, books);
		log.info("book("
				+ book.getName()
				+ ") updated by "
				+ ((User) request.getSession().getAttribute(ATTR_NAME_USER))
						.getName());
		String page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.BOOK_PAGE_PATH);

		return page;
	}

}
