package by.bsuir.library.command.navigation;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.manager.ConfigurationManager;

public class MainCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.MAIN_PAGE_PATH);
		return page;
	}

}
