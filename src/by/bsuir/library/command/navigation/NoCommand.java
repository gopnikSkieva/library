package by.bsuir.library.command.navigation;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.manager.ConfigurationManager;

public class NoCommand implements ICommand {

	public String execute(HttpServletRequest request) {
		String page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.INDEX_PAGE_PATH);
		return page;
	}
}
