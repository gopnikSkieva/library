package by.bsuir.library.command.navigation;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;

public class RefCommand implements ICommand {

	private static final String PAGE_REFERRER = "ref";

	@Override
	public String execute(HttpServletRequest request) {
		return PAGE_REFERRER;
	}

}
