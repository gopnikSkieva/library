package by.bsuir.library.command.factory;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.command.book.CreateBook;
import by.bsuir.library.command.book.CreateBookView;
import by.bsuir.library.command.book.DeleteBookCommand;
import by.bsuir.library.command.book.GetBooksCommand;
import by.bsuir.library.command.book.RemoveBooksFromListCommand;
import by.bsuir.library.command.book.SearchBook;
import by.bsuir.library.command.book.UpdateBook;
import by.bsuir.library.command.book.UpdateBookViewCommand;
import by.bsuir.library.command.book.ViewBooksCommand;
import by.bsuir.library.command.book.ViewBooksSwitchPageCommand;
import by.bsuir.library.command.navigation.MainCommand;
import by.bsuir.library.command.navigation.NoCommand;
import by.bsuir.library.command.navigation.RefCommand;
import by.bsuir.library.command.subscription.admin.AdminApproveReqCommand;
import by.bsuir.library.command.subscription.admin.AdminDenyBooks;
import by.bsuir.library.command.subscription.admin.AdminDenyRequest;
import by.bsuir.library.command.subscription.admin.AdminViewReqCommand;
import by.bsuir.library.command.subscription.admin.AdminViewRequest;
import by.bsuir.library.command.subscription.user.AddSubscrCommand;
import by.bsuir.library.command.subscription.user.ReturnBookCommand;
import by.bsuir.library.command.subscription.user.ViewUserSubscrCommand;
import by.bsuir.library.command.user.LoginCommand;
import by.bsuir.library.command.user.LogoutCommand;

public class CommandFactory {

	private static CommandFactory instance = null;
	private static final String PARAM_NAME_COMMAND = "command";
	private static final String ATTR_NAME_ID = "numparam";
	HashMap<String, ICommand> commands = new HashMap<String, ICommand>();

	private CommandFactory() {
		commands.put("login", new LoginCommand());
		commands.put("main", new MainCommand());
		commands.put("logout", new LogoutCommand());
		commands.put("viewbooks", new ViewBooksCommand());
		commands.put("viewbooksswitchpage", new ViewBooksSwitchPageCommand());
		commands.put("updatebook", new UpdateBookViewCommand());
		commands.put("deletebook", new DeleteBookCommand());
		commands.put("getbooks", new GetBooksCommand());
		commands.put("removefromlist", new RemoveBooksFromListCommand());
		commands.put("addsubscr", new AddSubscrCommand());
		commands.put("addread", new AddSubscrCommand());
		commands.put("viewusersubscr", new ViewUserSubscrCommand());
		commands.put("viewpostedsubscrs", new AdminViewReqCommand());
		commands.put("givebookstousers", new AdminApproveReqCommand());
		commands.put("approverequest", new AdminApproveReqCommand());
		commands.put("returnbooks", new ReturnBookCommand());
		commands.put("viewrequest", new AdminViewRequest());
		commands.put("rembookfromrequest", new AdminDenyBooks());
		commands.put("denyrequest", new AdminDenyRequest());
		commands.put("createbookview", new CreateBookView());
		commands.put("updatebookview", new UpdateBookViewCommand());
		commands.put("bookupdat", new UpdateBook());
		commands.put("bookadd", new CreateBook());
		commands.put("booksearch", new SearchBook());
		commands.put("ref", new RefCommand());
	}

	public ICommand getCommand(HttpServletRequest request) {
		ICommand command = null;

		command = lookupCommand(request);

		if (command == null) {
			command = new NoCommand();
		}
		return command;
	}

	public static CommandFactory getInstance() {
		if (instance == null) {
			instance = new CommandFactory();
		}
		return instance;
	}

	private String getSubParam(String param) {
		String id = param.substring(PARAM_NAME_COMMAND.length());
		return (id.length() > 0) ? id : null;
	}

	private ICommand lookupCommand(HttpServletRequest request) {
		Enumeration<?> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String param = (String) params.nextElement();
			if (param.startsWith(PARAM_NAME_COMMAND)) {
				String action = request.getParameter(param);
				ICommand command = commands.get(action);
				String sub = getSubParam(param);
				if (sub != null)
					request.setAttribute(ATTR_NAME_ID, Integer.parseInt(sub));
				return command;
			}
		}
		return null;
	}
}
