package by.bsuir.library.command.subscription.admin;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.subscription.SubscriptionRequestDAO;
import by.bsuir.library.dao.user.UserDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.subscription.Subscription;
import by.bsuir.library.model.subscription.SubscriptionList;
import by.bsuir.library.model.user.UserList;

public class AdminViewReqCommand implements ICommand {

	private static final String ATTR_NAME_REQ_LIST = "reqs";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;

		SubscriptionRequestDAO srdao = new SubscriptionRequestDAO();
		UserDAO udao = new UserDAO();
		SubscriptionList slist = srdao.findAll();
		UserList ulist = new UserList();
		ulist.addAll(udao.findAll());
		for (Subscription s : slist.getSubs()) {
			s.setUser(ulist.getById(s.getUserId()));
		}
		request.getSession().setAttribute(ATTR_NAME_REQ_LIST, slist);
		page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.REQUEST_PAGE_PATH);

		return page;
	}

}
