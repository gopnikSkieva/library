package by.bsuir.library.command.subscription.admin;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.subscription.SubscriptionDAO;
import by.bsuir.library.dao.subscription.SubscriptionRequestDAO;
import by.bsuir.library.dao.user.UserDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.subscription.Subscription;
import by.bsuir.library.model.subscription.SubscriptionList;
import by.bsuir.library.model.user.UserList;

public class AdminApproveReqCommand implements ICommand {

	private static final String ATTR_NAME_REQUEST = "bookrequest";
	private static final String ATTR_NAME_ERROR_COUNT = "counterror";
	private static final String ATTR_NAME_REQ_LIST = "reqs";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		Subscription sub;

		sub = (Subscription) request.getSession().getAttribute(
				ATTR_NAME_REQUEST);
		if (sub.containsZeroBooks()) {
			request.setAttribute(ATTR_NAME_ERROR_COUNT, true);
			page = ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.VIEW_REQUEST_PAGE_PATH);
		} else {
			SubscriptionDAO sdao = new SubscriptionDAO();
			SubscriptionRequestDAO srdao = new SubscriptionRequestDAO();
			sdao.insert(sub);
			srdao.delete(sub.getId());
			UserDAO udao = new UserDAO();
			SubscriptionList slist = srdao.findAll();
			UserList ulist = new UserList();
			ulist.addAll(udao.findAll());
			for (Subscription s : slist.getSubs()) {
				s.setUser(ulist.getById(s.getUserId()));
			}
			request.getSession().setAttribute(ATTR_NAME_REQ_LIST, slist);
			request.getSession().setAttribute(ATTR_NAME_REQUEST, null);
			page = ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.REQUEST_PAGE_PATH);
		}
		return page;
	}

}
