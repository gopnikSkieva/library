package by.bsuir.library.command.subscription.admin;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.subscription.SubscriptionRequestDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.subscription.Subscription;

public class AdminViewRequest implements ICommand {

	private static final String ATTR_NAME_ID = "numparam";
	private static final String ATTR_NAME_REQUEST = "bookrequest";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;

		SubscriptionRequestDAO srdao = new SubscriptionRequestDAO();
		int val = (Integer) request.getAttribute(ATTR_NAME_ID);

		Subscription sub = srdao.findById(val);
		request.getSession().setAttribute(ATTR_NAME_REQUEST, sub);

		page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.VIEW_REQUEST_PAGE_PATH);

		return page;
	}

}
