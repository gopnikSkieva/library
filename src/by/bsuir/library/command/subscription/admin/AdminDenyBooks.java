package by.bsuir.library.command.subscription.admin;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.subscription.SubscriptionRequestDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.subscription.Subscription;
import by.bsuir.library.model.subscription.SubscriptionList;

public class AdminDenyBooks implements ICommand {

	private static final String ATTR_NAME_REQ_LIST = "reqs";
	private static final String ATTR_NAME_REQUEST = "bookrequest";
	private static final String PARAM_NAME_CHECK_BOX = "cb";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String select[] = request.getParameterValues(PARAM_NAME_CHECK_BOX);
		Subscription sub;
		sub = (Subscription) request.getSession().getAttribute(
				ATTR_NAME_REQUEST);

		SubscriptionRequestDAO srdao = new SubscriptionRequestDAO();
		if (select != null && select.length != 0) {
			System.out.println("You have selected: ");
			for (int i = 0; i < select.length; i++) {
				srdao.deleteBook(Integer.parseInt(select[i]), sub.getId());
			}
		}

		if (sub.getBooks().size() == select.length) {

			request.getSession().setAttribute(ATTR_NAME_REQUEST, null);
			srdao.deleteVoid();
			SubscriptionList slist = srdao.findAll();
			request.getSession().setAttribute(ATTR_NAME_REQ_LIST, slist);
			page = ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.REQUEST_PAGE_PATH);
		} else {
			sub = srdao.findById(sub.getId());
			request.getSession().setAttribute(ATTR_NAME_REQUEST, sub);
			page = ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.VIEW_REQUEST_PAGE_PATH);
		}

		return page;
	}

}
