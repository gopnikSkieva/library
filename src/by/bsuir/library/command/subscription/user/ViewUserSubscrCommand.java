package by.bsuir.library.command.subscription.user;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.subscription.SubscriptionDAO;
import by.bsuir.library.dao.subscription.SubscriptionRequestDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.subscription.SubscriptionList;
import by.bsuir.library.model.user.User;

public class ViewUserSubscrCommand implements ICommand {

	private static final String ATTR_NAME_USER = "user";
	private static final String ATTR_NAME_REQ_LIST = "reqs";
	private static final String ATTR_NAME_SUBSCR_LIST = "subscrs";

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		SubscriptionRequestDAO srdao = new SubscriptionRequestDAO();
		SubscriptionList srlist = srdao.findAllByUser((User) request
				.getSession().getAttribute(ATTR_NAME_USER));
		request.setAttribute(ATTR_NAME_REQ_LIST, srlist);
		SubscriptionDAO sdao = new SubscriptionDAO();
		SubscriptionList slist = sdao.findAllByUser((User) request.getSession()
				.getAttribute(ATTR_NAME_USER));
		request.setAttribute(ATTR_NAME_SUBSCR_LIST, slist);
		page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.USER_SUBSCR_PAGE_PATH);

		return page;
	}

}
