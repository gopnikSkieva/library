package by.bsuir.library.command.subscription.user;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.book.BookDAO;
import by.bsuir.library.dao.subscription.SubscriptionDAO;
import by.bsuir.library.dao.subscription.SubscriptionRequestDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.book.Book;
import by.bsuir.library.model.subscription.SubscriptionList;
import by.bsuir.library.model.user.User;

public class ReturnBookCommand implements ICommand {

	private static final String ATTR_NAME_USER = "user";
	private static final String ATTR_NAME_REQ_LIST = "reqs";
	private static final String ATTR_NAME_SUBSCR_LIST = "subscrs";
	private static final String PARAM_NAME_CHECK_BOX = "cb";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		User user = (User) request.getSession().getAttribute(ATTR_NAME_USER);
		SubscriptionRequestDAO srdao = new SubscriptionRequestDAO();
		BookDAO bdao = new BookDAO();
		Book book = null;
		SubscriptionDAO sdao = new SubscriptionDAO();
		SubscriptionList srlist = srdao.findAllByUser(user);
		String select[] = request.getParameterValues(PARAM_NAME_CHECK_BOX);
		if (select != null && select.length != 0) {
			for (int i = 0; i < select.length; i++) {
				book = bdao.findByPrimaryKey(Integer.parseInt(select[i]));
				sdao.deleteBook(Integer.parseInt(select[i]), user);
				bdao.inc(Integer.parseInt(select[i]));
				log.info("book(" + book.getName() + ") returned by "
						+ user.getName());
			}
			sdao.deleteVoid();
		}

		request.setAttribute(ATTR_NAME_REQ_LIST, srlist);
		SubscriptionList slist = sdao.findAllByUser(user);

		request.setAttribute(ATTR_NAME_SUBSCR_LIST, slist);
		page = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.USER_SUBSCR_PAGE_PATH);

		return page;
	}

}
