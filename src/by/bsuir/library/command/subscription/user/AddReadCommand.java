package by.bsuir.library.command.subscription.user;

import java.sql.Date;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.subscription.SubscriptionRequestDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.book.BookTable;
import by.bsuir.library.model.subscription.SubscrType;
import by.bsuir.library.model.subscription.Subscription;
import by.bsuir.library.model.user.User;

public class AddReadCommand implements ICommand {

	private static final String ATTR_NAME_USER = "user";
	private static final String ATTR_NAME_REQ_BOOKS_LIST = "reqbooks";
	private static final String ATTR_NAME_ERROR_COUNT = "counterror";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		BookTable resBookTable = new BookTable();
		User user = (User) request.getSession().getAttribute(ATTR_NAME_USER);
		resBookTable = (BookTable) request.getSession().getAttribute(
				ATTR_NAME_REQ_BOOKS_LIST);
		if (resBookTable.requestedUserBooks(user).size() > 0) {
			request.setAttribute(ATTR_NAME_ERROR_COUNT, true);
			page = ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.SUBSCR_PAGE_PATH);
		} else {
			SubscriptionRequestDAO sub = new SubscriptionRequestDAO();
			Date date = new Date(GregorianCalendar.getInstance()
					.getTimeInMillis());
			sub.insert(new Subscription(user.getId(), SubscrType.ZALA,
					date, resBookTable.getBooks()));
			log.info("request added by "
					+ user.getName());
			page = ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.MAIN_PAGE_PATH);
			request.getSession().setAttribute(ATTR_NAME_REQ_BOOKS_LIST, null);
		}
		return page;
	}

}
