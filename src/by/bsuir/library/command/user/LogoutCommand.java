package by.bsuir.library.command.user;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.user.User;

public class LogoutCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		User user = new User("guest");
		request.getSession().setAttribute("user", user);
		page = ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.MAIN_PAGE_PATH);
		return page;
	}
}
