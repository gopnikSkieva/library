package by.bsuir.library.command.user;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.library.command.ICommand;
import by.bsuir.library.dao.user.UserDAO;
import by.bsuir.library.manager.ConfigurationManager;
import by.bsuir.library.model.user.User;

public class LoginCommand implements ICommand {

	private static final String ATTR_NAME_ERROR_MESSAGE = "errorMessage";
	private static final String PARAM_NAME_LOGIN = "login";
	private static final String PARAM_NAME_PASSWORD = "password";

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		String login = request.getParameter(PARAM_NAME_LOGIN);
		String password = request.getParameter(PARAM_NAME_PASSWORD);

		User user = new UserDAO().findUserByLogin(login);
		if (user.getPassword().equals(password)) {
			user.setLogged(true);
			request.getSession().setAttribute("user", user);
			page = ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.MAIN_PAGE_PATH);
			request.setAttribute(ATTR_NAME_ERROR_MESSAGE,
			null);
		} else {
			user.setLogged(false);
			user.setPassword(password);

			request.setAttribute(ATTR_NAME_ERROR_MESSAGE,
					"wrong login or password");
			 
			page = ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.INDEX_PAGE_PATH);
		}

		return page;
	}
}
