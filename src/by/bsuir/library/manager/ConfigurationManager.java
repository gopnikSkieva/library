package by.bsuir.library.manager;

import java.util.ResourceBundle;

public class ConfigurationManager {
	private static ConfigurationManager instance;
	private ResourceBundle resourceBundle;

	private static final String BUNDLE_NAME = "properties/config";

	public static final String DATABASE_DRIVER_NAME = "DATABASE_DRIVER_NAME";
	public static final String DATABASE_URL = "DATABASE_URL";
	public static final String ERROR_PAGE_PATH = "ERROR_PAGE_PATH";
	public static final String INDEX_PAGE_PATH = "INDEX_PAGE_PATH";
	public static final String MAIN_PAGE_PATH = "MAIN_PAGE_PATH";
	public static final String BOOK_PAGE_PATH = "BOOK_PAGE_PATH";
	public static final String SUBSCR_PAGE_PATH = "SUBSCR_PAGE_PATH";
	public static final String USER_PAGE_PATH = "USER_PAGE_PATH";
	public static final String USER_SUBSCR_PAGE_PATH = "USER_SUBSCR_PAGE_PATH";
	public static final String REQUEST_PAGE_PATH = "REQUEST_PAGE_PATH";
	public static final String VIEW_REQUEST_PAGE_PATH = "VIEW_REQUEST_PAGE_PATH";
	public static final String EDIT_BOOK_PAGE_PATH = "EDIT_BOOK_PAGE_PATH";
	
	public static ConfigurationManager getInstance() {
		if (instance == null) {
			instance = new ConfigurationManager();
			instance.resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
		}
		return instance;
	}

	public String getProperty(String key) {
		return (String) resourceBundle.getObject(key);
	}
}
