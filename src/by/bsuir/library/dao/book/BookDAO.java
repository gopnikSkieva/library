package by.bsuir.library.dao.book;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.bsuir.library.dao.AbstractDAO;
import by.bsuir.library.model.book.Book;

public class BookDAO extends AbstractDAO {

	private static Logger log = Logger.getLogger(BookDAO.class);
	
	private static final String INSERT_BOOK = "INSERT INTO book(name, author, quantity) VALUES(?,?,?)";
	private static final String UPDATE_BOOK = "UPDATE book SET name=?, author=?, quantity=? WHERE idbook=?";
	private static final String DELETE_BOOK_BY_ID = "DELETE FROM book WHERE idbook=?";
	private static final String FIND_ALL = "SELECT * FROM book";
	private static final String FIND_BY_KEY = "SELECT * FROM book WHERE idbook=?";
	private static final String FIND_BY_AUTHOR = "SELECT * FROM book WHERE author=?";
	private static final String FIND_BY_AUTHOR_PARTIAL = "SELECT * FROM book WHERE author LIKE ?";
	private static final String FIND_BY_NAME_AND_AUTHOR_PARTIAL = "SELECT * FROM book WHERE (name LIKE ?) AND (author LIKE ?)";
	private static final String FIND_BY_NAME = "SELECT * FROM book WHERE name=?";
	private static final String FIND_BY_NAME_PARTIAL = "SELECT * FROM book WHERE name LIKE ?";
	private static final String INC_BY_ID = "UPDATE book SET quantity=quantity+1 WHERE idbook=?";
	private static final String DEC_BY_ID = "UPDATE book SET quantity=quantity-1 WHERE idbook=?";

	private static final String COL_BOOK_ID = "idbook";
	private static final String COL_BOOK_NAME = "name";
	private static final String COL_BOOK_AUTHOR = "author";
	private static final String COL_BOOK_QUANTITY = "quantity";

	public void insert(Book book) {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(INSERT_BOOK);
			pStmnt.setString(1, book.getName());
			pStmnt.setString(2, book.getAuthor());
			pStmnt.setInt(3, book.getQuantity());
			pStmnt.executeUpdate();
		} catch (SQLException e) {
			log.warn("SQLException at book insert()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null) {
					cp.closeConnection(connection);
				}
			} catch (SQLException e) {
				log.warn("SQLException at book insert()", e);
			} catch (Exception e) {
				log.warn("Exception at book insert()", e);
			}

		}
	}

	public void update(int pk, Book book) {
		PreparedStatement ptmt = null;
		try {
			connection = getConnection();
			ptmt = connection.prepareStatement(UPDATE_BOOK);
			ptmt.setString(1, book.getName());
			ptmt.setString(2, book.getAuthor());
			ptmt.setInt(3, book.getQuantity());
			ptmt.setInt(4, pk);
			ptmt.executeUpdate();
		} catch (SQLException e) {
			log.warn("SQLException at book update()", e);
		} finally {
			try {
				if (ptmt != null)
					ptmt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at book update()", e);
			} catch (Exception e) {
				log.warn("Exception at book update()", e);

			}
		}
	}

	public void delete(int pk) {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(DELETE_BOOK_BY_ID);
			pStmnt.setInt(1, pk);
			pStmnt.executeUpdate();
		} catch (SQLException e) {
			log.warn("SQLException at book delete()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at book delete()", e);
			} catch (Exception e) {
				log.warn("Exception at book delete()", e);
			}

		}
	}

	public void inc(int pk) {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(INC_BY_ID);
			pStmnt.setInt(1, pk);
			pStmnt.executeUpdate();
		} catch (SQLException e) {
			log.warn("SQLException at book inc()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at book inc()", e);
			} catch (Exception e) {
				log.warn("Exception at book inc()", e);
			}

		}
	}

	public void dec(int pk) {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(DEC_BY_ID);
			pStmnt.setInt(1, pk);
			pStmnt.executeUpdate();
		} catch (SQLException e) {
			log.warn("SQLException at book dec()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at book dec()", e);
			} catch (Exception e) {
				log.warn("Exception at book dec()", e);
			}

		}
	}

	public List<Book> findAll() {
		PreparedStatement pStmnt = null;
		ResultSet resultSet = null;
		List<Book> res = new ArrayList<Book>();
		Book book = null;
		String name = null;
		String author = null;
		int pk;
		int quantity;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(FIND_ALL);
			resultSet = pStmnt.executeQuery();
			while (resultSet.next()) {
				pk = resultSet.getInt(COL_BOOK_ID);
				name = resultSet.getString(COL_BOOK_NAME);
				author = resultSet.getString(COL_BOOK_AUTHOR);
				quantity = resultSet.getInt(COL_BOOK_QUANTITY);
				book = new Book(pk, name, author, quantity);
				res.add(book);
			}
		} catch (SQLException e) {
			log.warn("SQLException at book findAll()", e);
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at book findAll()", e);
			} catch (Exception e) {
				log.warn("Exception at book findAll()", e);
			}

		}
		return res;
	}

	public Book findByPrimaryKey(int sPk) {
		PreparedStatement pStmnt = null;
		ResultSet resultSet = null;
		Book book = null;
		String name = null;
		String author = null;
		int pk;
		int quantity;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(FIND_BY_KEY);
			pStmnt.setInt(1, sPk);
			resultSet = pStmnt.executeQuery();
			if (resultSet.next()) {
				pk = resultSet.getInt(COL_BOOK_ID);
				name = resultSet.getString(COL_BOOK_NAME);
				author = resultSet.getString(COL_BOOK_AUTHOR);
				quantity = resultSet.getInt(COL_BOOK_QUANTITY);
				book = new Book(pk, name, author, quantity);
			}
		} catch (SQLException e) {
			log.warn("SQLException at book findByPrimaryKey()", e);
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at book findByPrimaryKey()", e);
			} catch (Exception e) {
				log.warn("Exception at book findByPrimaryKey()", e);
			}

		}
		return book;
	}

	public List<Book> findByAuthor(String sAuthor, boolean isPartial) {
		PreparedStatement pStmnt = null;
		ResultSet resultSet = null;
		List<Book> res = new ArrayList<Book>();
		Book book = null;
		String name = null;
		String author = null;
		int pk;
		int quantity;
		try {
			connection = getConnection();
			if (isPartial) {
				pStmnt = connection.prepareStatement(FIND_BY_AUTHOR_PARTIAL);
				pStmnt.setString(1, "%" + sAuthor + "%");
			} else {
				pStmnt = connection.prepareStatement(FIND_BY_AUTHOR);
				pStmnt.setString(1, sAuthor);
			}
			resultSet = pStmnt.executeQuery();
			while (resultSet.next()) {
				pk = resultSet.getInt(COL_BOOK_ID);
				name = resultSet.getString(COL_BOOK_NAME);
				author = resultSet.getString(COL_BOOK_AUTHOR);
				quantity = resultSet.getInt(COL_BOOK_QUANTITY);
				book = new Book(pk, name, author, quantity);
				res.add(book);
			}
		} catch (SQLException e) {
			log.warn("SQLException at book findByAuthor()", e);
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at book findByAuthor()", e);
			} catch (Exception e) {
				log.warn("Exception at book findByAuthor()", e);
			}

		}
		return res;
	}

	public List<Book> findByName(String sName, boolean isPartial) {
		PreparedStatement pStmnt = null;
		ResultSet resultSet = null;
		List<Book> res = new ArrayList<Book>();
		Book book = null;
		String name = null;
		String author = null;
		int pk;
		int quantity;
		try {
			connection = getConnection();
			if (isPartial) {
				String queryString = FIND_BY_NAME_PARTIAL;
				pStmnt = connection.prepareStatement(queryString);
				pStmnt.setString(1, "%" + sName + "%");
			} else {
				String queryString = FIND_BY_NAME;
				pStmnt = connection.prepareStatement(queryString);
				pStmnt.setString(1, sName);
			}
			resultSet = pStmnt.executeQuery();
			while (resultSet.next()) {
				pk = resultSet.getInt(COL_BOOK_ID);
				name = resultSet.getString(COL_BOOK_NAME);
				author = resultSet.getString(COL_BOOK_AUTHOR);
				quantity = resultSet.getInt(COL_BOOK_QUANTITY);
				book = new Book(pk, name, author, quantity);
				res.add(book);
			}
		} catch (SQLException e) {
			log.warn("SQLException at book findByName()", e);
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at book findByName()", e);
			} catch (Exception e) {
				log.warn("Exception at book findByName()", e);
			}

		}
		return res;
	}

	public List<Book> findByNameAndAuthor(String sName, String sAuthor) {
		PreparedStatement pStmnt = null;
		ResultSet resultSet = null;
		List<Book> res = new ArrayList<Book>();
		try {
			connection = getConnection();
			pStmnt = connection
					.prepareStatement(FIND_BY_NAME_AND_AUTHOR_PARTIAL);
			pStmnt.setString(1, "%" + sName + "%");
			pStmnt.setString(2, "%" + sAuthor + "%");
			resultSet = pStmnt.executeQuery();
			while (resultSet.next()) {
				int pk = resultSet.getInt(COL_BOOK_ID);
				String name = resultSet.getString(COL_BOOK_NAME);
				String author = resultSet.getString(COL_BOOK_AUTHOR);
				int quantity = resultSet.getInt(COL_BOOK_QUANTITY);
				Book book = new Book(pk, name, author, quantity);
				res.add(book);
			}
		} catch (SQLException e) {
			log.warn("SQLException at book findByNameAndAuthor()", e);
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at book findByNameAndAuthor()", e);
			} catch (Exception e) {
				log.warn("Exception at book findByNameAndAuthor()", e);
			}

		}
		return res;
	}
}
