package by.bsuir.library.dao.user;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.bsuir.library.dao.AbstractDAO;
import by.bsuir.library.model.user.User;
import by.bsuir.library.model.user.UserType;

public class UserDAO extends AbstractDAO {

	private static Logger log = Logger.getLogger(UserDAO.class);
	
	private static final String GET_USER_BY_LOGIN = "SELECT * FROM user WHERE user_name = ?";
	private static final String GET_USER_TYPE_BY_TYPE_ID = "SELECT type_name FROM user_type WHERE iduser_type = ?";
	private static final String FIND_ALL = "SELECT * FROM user";

	private static final String COL_USER_NAME = "user_name";
	private static final String COL_PWD = "pwd";
	private static final String COL_USER_ID = "iduser";
	private static final String COL_USER_TYPE_ID = "iduser_type";
	private static final String COL_TYPE_NAME = "type_name";

	public UserDAO() {
		super();
	}

	public User findUserByLogin(String login) {
		User user = null;
		PreparedStatement pStmnt = null;
		PreparedStatement typePStmnt = null;
		ResultSet resultSet = null;
		ResultSet typeResultSet = null;
		String name = null;
		String pwd = null;
		int id;
		int type;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(GET_USER_BY_LOGIN);
			pStmnt.setString(1, login);
			resultSet = pStmnt.executeQuery();
			if (resultSet.next()) {
				name = resultSet.getString(COL_USER_NAME);
				user = new User(name);
				pwd = resultSet.getString(COL_PWD);
				user.setPassword(pwd);
				id = resultSet.getInt(COL_USER_ID);
				user.setId(id);
				type = resultSet.getInt(COL_USER_TYPE_ID);
				typePStmnt = connection
						.prepareStatement(GET_USER_TYPE_BY_TYPE_ID);
				typePStmnt.setInt(1, type);
				typeResultSet = typePStmnt.executeQuery();
				if (typeResultSet.next()) {
					String typeStr = typeResultSet.getString(COL_TYPE_NAME);
					user.setType(UserType.valueOf(typeStr.toUpperCase()));
				}
			} else {
				user = new User(login);
			}
		} catch (SQLException e) {
			log.warn("SQLException at user findUserByLogin()", e);
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (typeResultSet != null)
					typeResultSet.close();
				if (pStmnt != null)
					pStmnt.close();
				if (typePStmnt != null)
					typePStmnt.close();
				if (connection != null) {
					cp.closeConnection(connection);
				}
			} catch (SQLException e) {
				log.warn("SQLException at user findUserByLogin()", e);
			} catch (Exception e) {
				log.warn("Exception at user findUserByLogin()", e);
			}
		}
		return user;
	}

	public List<User> findAll() {
		PreparedStatement pStmt = null;
		ResultSet resultSet = null;
		User user = null;
		List<User> res = new ArrayList<User>();
		String name = null;
		String pwd = null;
		int id;
		try {
			connection = getConnection();
			pStmt = connection.prepareStatement(FIND_ALL);
			resultSet = pStmt.executeQuery();
			while (resultSet.next()) {
				name = resultSet.getString(COL_USER_NAME);
				user = new User(name);
				pwd = resultSet.getString(COL_PWD);
				user.setPassword(pwd);
				id = resultSet.getInt(COL_USER_ID);
				user.setId(id);
				res.add(user);
			}
		} catch (SQLException e) {
			log.warn("SQLException at user findAll()", e);
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (pStmt != null)
					pStmt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at user findAll()", e);
			} catch (Exception e) {
				log.warn("Exception at user findAll()", e);
			}

		}
		return res;
	}

}
