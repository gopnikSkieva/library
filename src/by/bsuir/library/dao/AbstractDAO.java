package by.bsuir.library.dao;

import java.sql.Connection;
import java.sql.SQLException;

import by.bsuir.library.connectionpool.ConnectionPool;

public abstract class AbstractDAO {
	protected Connection connection = null;
	protected ConnectionPool cp = null;

	public AbstractDAO() {
		super();
		ConnectionPool.init();
		cp = ConnectionPool.getInstance();
	}

	protected Connection getConnection() throws SQLException {
		return cp.getConnection();
	}
}
