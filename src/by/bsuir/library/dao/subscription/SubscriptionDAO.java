package by.bsuir.library.dao.subscription;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import by.bsuir.library.dao.AbstractDAO;
import by.bsuir.library.dao.book.BookDAO;
import by.bsuir.library.model.book.Book;
import by.bsuir.library.model.subscription.SubscrType;
import by.bsuir.library.model.subscription.Subscription;
import by.bsuir.library.model.subscription.SubscriptionList;
import by.bsuir.library.model.user.User;

public class SubscriptionDAO extends AbstractDAO {

	private static Logger log = Logger.getLogger(SubscriptionDAO.class);

	private final static String INSERT_SUBSCR_QUERY = "INSERT INTO subscription(iduser, idsubscr_type, open_date) VALUES(?,?,?)";
	private final static String INSERT_SUBSCR_BOOK_QUERY = "INSERT INTO subscription_book(idsubscription, idbook) VALUES(?,?)";
	private final static String UPDATE_SUBSCR_QUERY = "UPDATE subscription SET iduser = ? idsubscr_type = ? open_date=? WHERE idsubscription=?";
	private final static String DELETE_SUBSCR_QUERY = "DELETE FROM subscription WHERE idsubscription=?";
	private final static String DELETE_SUBSCR_BOOK_QUERY = "DELETE FROM subscription_book WHERE idsubscription=?";
	private final static String DELETE_MULT_SUBSCR_BOOK_QUERY = "DELETE FROM subscription_book WHERE idbook=? AND idsubscription IN(SELECT idsubscription FROM subscription WHERE iduser=?)";
	private final static String FIND_ALL_BY_USER = "SELECT * FROM subscription WHERE iduser=?";
	private final static String FIND_ALL_BOOK_BY_SUBSCR = "SELECT * FROM subscription_book WHERE idsubscription=?";
	private final static String FIND_ALL = "SELECT * FROM subscription";
	private final static String DELETE_VOID_SUBSCR = "DELETE FROM subscription WHERE NOT EXISTS (SELECT idbook FROM subscription_book WHERE subscription.idsubscription=subscription_book.idsubscription)";

	private final static String COL_SUBSCR_ID = "idsubscription";
	private final static String COL_USER_ID = "iduser";
	private final static String COL_SUBSCR_TYPE_ID = "idsubscr_type";
	private final static String COL_BOOK_ID = "idbook";
	private final static String COL_OPEN_DATE = "open_date";

	public void insert(Subscription subscr) {
		PreparedStatement pStmnt = null;
		ResultSet resultSet = null;
		BookDAO bdao = new BookDAO();
		int subscrId = 0;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(INSERT_SUBSCR_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			pStmnt.setInt(1, subscr.getUserId());
			pStmnt.setInt(2, subscr.getSubscription().value());
			pStmnt.setDate(3, subscr.getOpenDate());
			pStmnt.executeUpdate();
			resultSet = pStmnt.getGeneratedKeys();
			if (resultSet.next()) {
				subscrId = resultSet.getInt(1);
			}
			for (Book book : subscr.getBooks()) {
				System.out.println("book added " + book.getId());
				bdao.dec(book.getId());
				pStmnt = connection.prepareStatement(INSERT_SUBSCR_BOOK_QUERY);
				pStmnt.setInt(1, subscrId);
				pStmnt.setInt(2, book.getId());
				pStmnt.executeUpdate();
			}
		} catch (SQLException e) {
			log.warn("SQLException at subscription insert()", e);
		} finally {
			try {
				if (pStmnt != null) {
					pStmnt.close();
				}
				if (connection != null) {
					cp.closeConnection(connection);
				}
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (SQLException e) {
				log.warn("SQLException at subscription insert()", e);
			} catch (Exception e) {
				log.warn("Exception at subscription insert()", e);
			}

		}
	}

	public void update(int pk, Subscription subscr) {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(UPDATE_SUBSCR_QUERY);
			pStmnt.setInt(1, subscr.getUserId());
			pStmnt.setInt(2, subscr.getSubscription().value());
			pStmnt.setDate(3, subscr.getOpenDate());
			pStmnt.setInt(4, subscr.getId());
			pStmnt.executeUpdate();
			pStmnt = connection.prepareStatement(DELETE_SUBSCR_BOOK_QUERY);
			pStmnt.setInt(2, subscr.getId());
			pStmnt.executeUpdate();
			for (Book book : subscr.getBooks()) {
				pStmnt = connection.prepareStatement(INSERT_SUBSCR_BOOK_QUERY);
				pStmnt.setInt(1, subscr.getId());
				pStmnt.setInt(2, book.getId());
				pStmnt.executeUpdate();
			}
		} catch (SQLException e) {
			log.warn("SQLException at subscription update()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			}

			catch (SQLException e) {
				log.warn("SQLException at subscription update()", e);
			} catch (Exception e) {
				log.warn("Exception at subscription update()", e);

			}
		}
	}

	public void delete(int pk) {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(DELETE_SUBSCR_QUERY);
			pStmnt.setInt(1, pk);
			pStmnt.executeUpdate();
			pStmnt = connection.prepareStatement(DELETE_SUBSCR_BOOK_QUERY);
			pStmnt.setInt(1, pk);
			pStmnt.executeUpdate();
		} catch (SQLException e) {
			log.warn("SQLException at subscription delete()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at subscription delete()", e);
			} catch (Exception e) {
				log.warn("Exception at subscription delete()", e);
			}

		}
	}

	public void deleteBook(int book, User user) {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(DELETE_MULT_SUBSCR_BOOK_QUERY);
			pStmnt.setInt(1, book);
			pStmnt.setInt(2, user.getId());
			pStmnt.executeUpdate();
		} catch (SQLException e) {
			log.warn("SQLException at subscription deleteBook()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at subscription deleteBook()", e);
			} catch (Exception e) {
				log.warn("Exception at subscription deleteBook()", e);
			}

		}
	}

	public void deleteVoid() {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(DELETE_VOID_SUBSCR);
			pStmnt.executeUpdate();
		} catch (SQLException e) {
			log.warn("SQLException at subscription deleteVoid()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at subscription deleteVoid()", e);
			} catch (Exception e) {
				log.warn("Exception at subscription deleteVoid()", e);
			}

		}
	}

	public SubscriptionList findAllByUser(User user) {
		PreparedStatement pStmnt = null;
		PreparedStatement bookPStmnt = null;
		ResultSet resultSet = null;
		ResultSet bookResultSet = null;
		SubscriptionList res = new SubscriptionList();
		Subscription subscr = null;
		int subscrID;
		int userId;
		int bookId;
		SubscrType subscription;
		Date openDate;
		BookDAO bdao = new BookDAO();
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(FIND_ALL_BY_USER);
			pStmnt.setInt(1, user.getId());
			resultSet = pStmnt.executeQuery();
			while (resultSet.next()) {
				subscrID = resultSet.getInt(COL_SUBSCR_ID);
				userId = resultSet.getInt(COL_USER_ID);
				subscription = getSubscrTypeEnum(resultSet
						.getInt(COL_SUBSCR_TYPE_ID));
				openDate = resultSet.getDate(COL_OPEN_DATE);
				subscr = new Subscription(userId, subscription, openDate, null);
				subscr.setId(subscrID);
				bookPStmnt = connection
						.prepareStatement(FIND_ALL_BOOK_BY_SUBSCR);
				bookPStmnt.setInt(1, subscr.getId());
				bookResultSet = bookPStmnt.executeQuery();

				while (bookResultSet.next()) {
					bookId = bookResultSet.getInt(COL_BOOK_ID);
					subscr.getBooks().add(bdao.findByPrimaryKey(bookId));
				}
				subscr.setId(subscrID);
				res.getSubs().add(subscr);
			}

		} catch (SQLException e) {
			log.warn("SQLException at subscription findAllByUser()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at subscription findAllByUser()", e);
			} catch (Exception e) {
				log.warn("Exception at subscription findAllByUser()", e);
			}

		}
		return res;
	}

	public SubscriptionList findAll() {
		PreparedStatement pStmnt = null;
		PreparedStatement bookPStmnt = null;
		ResultSet resultSet = null;
		ResultSet bookResultSet = null;
		SubscriptionList res = new SubscriptionList();
		Subscription subscr = null;
		int subscrID;
		int userId;
		int bookId;
		SubscrType subscription;
		Date openDate;
		BookDAO bdao = new BookDAO();
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(FIND_ALL);
			resultSet = pStmnt.executeQuery();
			while (resultSet.next()) {
				subscrID = resultSet.getInt(COL_SUBSCR_ID);
				userId = resultSet.getInt(COL_USER_ID);
				subscription = getSubscrTypeEnum(resultSet
						.getInt(COL_SUBSCR_TYPE_ID));
				openDate = resultSet.getDate(COL_OPEN_DATE);
				subscr = new Subscription(userId, subscription, openDate, null);
				subscr.setId(subscrID);
				bookPStmnt = connection
						.prepareStatement(FIND_ALL_BOOK_BY_SUBSCR);
				bookPStmnt.setInt(1, subscr.getId());
				bookResultSet = bookPStmnt.executeQuery();
				while (bookResultSet.next()) {
					bookId = bookResultSet.getInt(COL_BOOK_ID);
					subscr.getBooks().add(bdao.findByPrimaryKey(bookId));
				}
				subscr.setId(subscrID);
				res.getSubs().add(subscr);
			}

		} catch (SQLException e) {
			log.warn("SQLException at subscription findAll()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (SQLException e) {
				log.warn("SQLException at subscription findAll()", e);
			} catch (Exception e) {
				log.warn("SQLException at subscription findAll()", e);
			}

		}
		return res;
	}


	private SubscrType getSubscrTypeEnum(int st) {
		switch (st) {
		case 1:
			return SubscrType.HOME;
		case 2:
			return SubscrType.ZALA;
		default:
			throw new IllegalArgumentException();
		}
	}

}
