package by.bsuir.library.dao.subscription;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import by.bsuir.library.dao.AbstractDAO;
import by.bsuir.library.dao.book.BookDAO;
import by.bsuir.library.model.book.Book;
import by.bsuir.library.model.subscription.SubscrType;
import by.bsuir.library.model.subscription.Subscription;
import by.bsuir.library.model.subscription.SubscriptionList;
import by.bsuir.library.model.user.User;

public class SubscriptionRequestDAO extends AbstractDAO {

	private static Logger log = Logger.getLogger(SubscriptionRequestDAO.class);

	private final static String INSERT_SUBSCR_QUERY = "INSERT INTO request(iduser, idsubscr_type, open_date) VALUES(?,?,?)";
	private final static String INSERT_SUBSCR_BOOK_QUERY = "INSERT INTO request_book(idsubscription, idbook) VALUES(?,?)";
	private final static String UPDATE_SUBSCR_QUERY = "UPDATE request SET iduser = ? idsubscr_type = ? open_date=? WHERE idsubscription=?";
	private final static String DELETE_SUBSCR_QUERY = "DELETE FROM request WHERE idsubscription=?";
	private final static String DELETE_SUBSCR_BOOK_QUERY = "DELETE FROM request_book WHERE idsubscription=?";
	private final static String FIND_ALL_BY_USER = "SELECT * FROM request WHERE iduser=?";
	private final static String FIND_ALL_BOOK_BY_SUBSCR = "SELECT * FROM request_book WHERE idsubscription=?";
	private final static String FIND_ALL = "SELECT * FROM request";
	private final static String FIND_BY_ID = "SELECT * FROM request WHERE idsubscription=?";
	private final static String DELETE_BOOK_BY_SUBSCR = "DELETE FROM request_book WHERE idsubscription = ? AND idbook = ?";
	private final static String DELETE_VOID_SUBSCR = "DELETE FROM request WHERE NOT EXISTS (SELECT idbook FROM request_book WHERE request.idsubscription=request_book.idsubscription)";

	private final static String COL_SUBSCR_ID = "idsubscription";
	private final static String COL_USER_ID = "iduser";
	private final static String COL_SUBSCR_TYPE_ID = "idsubscr_type";
	private final static String COL_BOOK_ID = "idbook";
	private final static String COL_OPEN_DATE = "open_date";

	public void insert(Subscription subscr) {
		PreparedStatement pStmnt = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(INSERT_SUBSCR_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			pStmnt.setInt(1, subscr.getUserId());
			pStmnt.setInt(2, subscr.getSubscription().value());
			pStmnt.setDate(3, subscr.getOpenDate());
			pStmnt.executeUpdate();
			resultSet = pStmnt.getGeneratedKeys();
			int subscrId = 0;
			if (resultSet.next()) {
				subscrId = resultSet.getInt(1);
			}
			for (Book book : subscr.getBooks()) {
				pStmnt = connection.prepareStatement(INSERT_SUBSCR_BOOK_QUERY);
				pStmnt.setInt(1, subscrId);
				pStmnt.setInt(2, book.getId());
				pStmnt.executeUpdate();
			}
		} catch (SQLException e) {
			log.warn("SQLException at request insert()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null) {
					cp.closeConnection(connection);
				}
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (SQLException e) {
				log.warn("SQLException at request insert()", e);
			} catch (Exception e) {
				log.warn("Exception at request insert()", e);
			}

		}
	}

	public void update(int pk, Subscription subscr) {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(UPDATE_SUBSCR_QUERY);
			pStmnt.setInt(1, subscr.getUserId());
			pStmnt.setInt(2, subscr.getSubscription().value());
			pStmnt.setDate(3, subscr.getOpenDate());
			pStmnt.setInt(4, subscr.getId());
			pStmnt.executeUpdate();
			pStmnt = connection.prepareStatement(DELETE_SUBSCR_BOOK_QUERY);
			pStmnt.setInt(2, subscr.getId());
			pStmnt.executeUpdate();
			for (Book book : subscr.getBooks()) {
				pStmnt = connection.prepareStatement(INSERT_SUBSCR_BOOK_QUERY);
				pStmnt.setInt(1, subscr.getId());
				pStmnt.setInt(2, book.getId());
				pStmnt.executeUpdate();
			}
		} catch (SQLException e) {
			log.warn("SQLException at request update()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			}

			catch (SQLException e) {
				log.warn("SQLException at request update()", e);
			} catch (Exception e) {
				log.warn("Exception at request update()", e);

			}
		}
	}

	public void delete(int pk) {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(DELETE_SUBSCR_QUERY);
			pStmnt.setInt(1, pk);
			pStmnt.executeUpdate();
			pStmnt = connection.prepareStatement(DELETE_SUBSCR_BOOK_QUERY);
			pStmnt.setInt(1, pk);
			pStmnt.executeUpdate();
		} catch (SQLException e) {
			log.warn("SQLException at request delete()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at request delete()", e);
			} catch (Exception e) {
				log.warn("Exception at request delete()", e);
			}

		}
	}

	public void deleteBook(int book, int subId) {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(DELETE_BOOK_BY_SUBSCR);
			pStmnt.setInt(1, subId);
			pStmnt.setInt(2, book);
			pStmnt.executeUpdate();
		} catch (SQLException e) {
			log.warn("SQLException at request deleteBook()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at request deleteBook()", e);
			} catch (Exception e) {
				log.warn("Exception at request deleteBook()", e);
			}

		}
	}

	public void deleteVoid() {
		PreparedStatement pStmnt = null;
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(DELETE_VOID_SUBSCR);
			pStmnt.executeUpdate();
		} catch (SQLException e) {
			log.warn("SQLException at request deleteVoid()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at request deleteVoid()", e);
			} catch (Exception e) {
				log.warn("Exception at request deleteVoid()", e);
			}

		}
	}

	public SubscriptionList findAllByUser(User user) {
		PreparedStatement pStmnt = null;
		PreparedStatement bookPStmnt = null;
		ResultSet resultSet = null;
		ResultSet bookResultSet = null;
		SubscriptionList res = new SubscriptionList();
		Subscription subscr = null;
		int subscrID;
		int userId;
		int bookId;
		SubscrType subscription;
		Date openDate;
		BookDAO bdao = new BookDAO();
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(FIND_ALL_BY_USER);
			pStmnt.setInt(1, user.getId());
			resultSet = pStmnt.executeQuery();
			while (resultSet.next()) {
				subscrID = resultSet.getInt(COL_SUBSCR_ID);
				userId = resultSet.getInt(COL_USER_ID);
				subscription = getSubscrTypeEnum(resultSet
						.getInt(COL_SUBSCR_TYPE_ID));
				openDate = resultSet.getDate(COL_OPEN_DATE);
				subscr = new Subscription(userId, subscription, openDate, null);
				subscr.setId(subscrID);
				bookPStmnt = connection
						.prepareStatement(FIND_ALL_BOOK_BY_SUBSCR);
				bookPStmnt.setInt(1, subscr.getId());
				bookResultSet = bookPStmnt.executeQuery();
				while (bookResultSet.next()) {
					bookId = bookResultSet.getInt(COL_BOOK_ID);
					subscr.getBooks().add(bdao.findByPrimaryKey(bookId));
				}
				subscr.setId(subscrID);
				res.getSubs().add(subscr);
			}

		} catch (SQLException e) {
			log.warn("SQLException at request findAllByUser()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (SQLException e) {
				log.warn("SQLException at request findAllByUser()", e);
			} catch (Exception e) {
				log.warn("Exception at request findAllByUser()", e);
			}

		}
		return res;
	}

	public Subscription findById(int pk) {
		PreparedStatement pStmnt = null;
		PreparedStatement bookPStmnt = null;
		ResultSet resultSet = null;
		ResultSet bookResultSet = null;
		Subscription subscr = null;
		int userId;
		int bookId;
		SubscrType subscription;
		Date openDate;
		BookDAO bdao = new BookDAO();
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(FIND_BY_ID);
			pStmnt.setInt(1, pk);
			resultSet = pStmnt.executeQuery();
			resultSet.next();
			userId = resultSet.getInt(COL_USER_ID);
			subscription = getSubscrTypeEnum(resultSet
					.getInt(COL_SUBSCR_TYPE_ID));
			openDate = resultSet.getDate(COL_OPEN_DATE);
			subscr = new Subscription(userId, subscription, openDate, null);
			subscr.setId(pk);
			String bookQueryString = FIND_ALL_BOOK_BY_SUBSCR;
			bookPStmnt = connection.prepareStatement(bookQueryString);
			bookPStmnt.setInt(1, subscr.getId());
			bookResultSet = bookPStmnt.executeQuery();
			while (bookResultSet.next()) {
				bookId = bookResultSet.getInt(COL_BOOK_ID);
				subscr.getBooks().add(bdao.findByPrimaryKey(bookId));
			}

		} catch (SQLException e) {
			log.warn("SQLException at request findById()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (SQLException e) {
				log.warn("SQLException at request findById()", e);
			} catch (Exception e) {
				log.warn("Exception at request findById()", e);
			}

		}
		return subscr;
	}

	public SubscriptionList findAll() {
		PreparedStatement pStmnt = null;
		PreparedStatement bookPStmnt = null;
		ResultSet resultSet = null;
		ResultSet bookResultSet = null;
		SubscriptionList res = new SubscriptionList();
		Subscription subscr = null;
		int subscrID;
		int userId;
		int bookId;
		SubscrType subscription;
		Date openDate;
		BookDAO bdao = new BookDAO();
		try {
			connection = getConnection();
			pStmnt = connection.prepareStatement(FIND_ALL);
			resultSet = pStmnt.executeQuery();
			while (resultSet.next()) {
				subscrID = resultSet.getInt(COL_SUBSCR_ID);
				userId = resultSet.getInt(COL_USER_ID);
				subscription = getSubscrTypeEnum(resultSet
						.getInt(COL_SUBSCR_TYPE_ID));
				openDate = resultSet.getDate(COL_OPEN_DATE);
				subscr = new Subscription(userId, subscription, openDate, null);
				subscr.setId(subscrID);
				bookPStmnt = connection
						.prepareStatement(FIND_ALL_BOOK_BY_SUBSCR);
				bookPStmnt.setInt(1, subscr.getId());
				bookResultSet = bookPStmnt.executeQuery();
				while (bookResultSet.next()) {
					bookId = bookResultSet.getInt(COL_BOOK_ID);
					subscr.getBooks().add(bdao.findByPrimaryKey(bookId));
				}
				subscr.setId(subscrID);
				res.getSubs().add(subscr);
			}

		} catch (SQLException e) {
			log.warn("SQLException at request findAll()", e);
		} finally {
			try {
				if (pStmnt != null)
					pStmnt.close();
				if (connection != null)
					cp.closeConnection(connection);
			} catch (SQLException e) {
				log.warn("SQLException at request findAll()", e);
			} catch (Exception e) {
				log.warn("Exception at request findAll()", e);
			}

		}
		return res;
	}


	private SubscrType getSubscrTypeEnum(int st) {
		switch (st) {
		case 1:
			return SubscrType.HOME;
		case 2:
			return SubscrType.ZALA;
		default:
			throw new IllegalArgumentException();
		}
	}
}
