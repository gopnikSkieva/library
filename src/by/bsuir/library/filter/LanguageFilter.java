package by.bsuir.library.filter;

import java.io.IOException;

import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * Servlet Filter implementation class LanguageFilter
 */
public class LanguageFilter implements Filter {
	
	private static final String USER_LOCALE_ATTR = "userLocale";
	private static final String ENCODING = "UTF-8";
	private static final String LANG_PARAM = "lang";
	/**
	 * Default constructor.
	 */
	public LanguageFilter() {}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		req.setCharacterEncoding(ENCODING);
		if(req.getSession().getAttribute(USER_LOCALE_ATTR) == null){
			req.getSession().setAttribute(USER_LOCALE_ATTR, req.getLocale());
		}
		String lang = request.getParameter(LANG_PARAM);
		if (lang != null) {
			Locale locale = new Locale(lang);
			req.getSession().setAttribute(USER_LOCALE_ATTR, locale);
		} 
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {}

}
