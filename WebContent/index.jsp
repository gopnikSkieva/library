<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${userLocale}" />
<fmt:bundle basename="properties.text">
	<html>
<head>
<title><fmt:message key="library" /></title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div id=container>
		<div id="header"></div>

		<div id="user">
			<div id="lang">
				Language:
				<div id="langru">
					<a href="library?command=ref&lang=ru">русский</a>
				</div>
				<div id="langen">
					<a href="library?command=ref&lang=en">english</a>
				</div>
			</div>
			<c:if test="${!user.logged}">
				<fmt:message key="notlogged" />
			</c:if>
			<c:if test="${user.logged}">
				<fmt:message key="loggedas" />${user.name}, <c:if
					test="${user.type eq 'ADMIN'}">
					<fmt:message key="admin" />
				</c:if>
				<c:if test="${user.type eq 'USER'}">
					<fmt:message key="user" />
				</c:if>
				<a href="library?command=logout"><fmt:message key="logout" /> </a>
			</c:if>
		</div>
		<div id="navigation">

			<ul>
				<c:if test="${user.logged}">

					<li><a href="library?command=main"><fmt:message
								key="mainpage" /> </a>
					</li>
					<li><a href="library?command=viewbooks"><fmt:message
								key="bookslist" /> </a>
					</li>
					<li><a href="library?command=viewusersubscr"><fmt:message
								key="mysub" /> </a>
					</li>
				</c:if>
				<c:if test="${user.type eq 'ADMIN'}">
					<li><a href="library?command=viewpostedsubscrs"><fmt:message
								key="viewreq" /> </a>
					</li>
				</c:if>
			</ul>


		</div>

		<div id="content">

			<h2>
				<fmt:message key="welcomemsg" />
				!
			</h2>
			<p>
				<fmt:message key="pleaselogin" />
			</p>
			<form name="loginForm" method="POST" action="library">

				<fieldset>
					<legend>
						<fmt:message key="loginpass" />
					</legend>
					<input type="hidden" name="command" value="login" />
					<p>
						<label for="username"><fmt:message key="username" /> </label>
					</p>
					<p>
						<input name="login" value="admin" />
					</p>

					<p>

						<label for="password"><fmt:message key="pass" /> </label>
					</p>
					<p>

						<input type="password" name="password" value="admin" />
					</p>
					<p>

						<input type="submit" value="Enter">
					</p>

				</fieldset>
				<c:if test='${errorMessage!="not found"}'>
					<h5 style="color: red;">${errorMessage}</h5>
				</c:if>
			</form>
		</div>
		<div id="close"></div>

	</div>
</body>
	</html>
</fmt:bundle>