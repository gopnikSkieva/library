<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="errorpage.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${userLocale}" />
<fmt:bundle basename="properties.text">
	<html>
<c:if test="${!user.logged}">
	<c:redirect url="index.jsp" />
</c:if>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css">
<title>Library</title>
</head>
<script language="JavaScript" type="text/javascript">
	function returnselected() {
		document.subscrs.submit();
	}
</script>
<body>
	<div id=container>
		<div id="header"></div>

		<div id="user">
			<div id="lang">
				Language:
				<div id="langru">
					<a href="library?command=ref&lang=ru">русский</a>
				</div>
				<div id="langen">
					<a href="library?command=ref&lang=en">english</a>
				</div>
			</div>
			<c:if test="${!user.logged}">
				<fmt:message key="notlogged" />
			</c:if>
			<c:if test="${user.logged}">
				<fmt:message key="loggedas" />${user.name}, <c:if
					test="${user.type eq 'ADMIN'}">
					<fmt:message key="admin" />
				</c:if>
				<c:if test="${user.type eq 'USER'}">
					<fmt:message key="user" />
				</c:if>
				<a href="library?command=logout"><fmt:message key="logout" /> </a>
			</c:if>
		</div>
		<div id="navigation">

			<ul>
				<c:if test="${user.logged}">

					<li><a href="library?command=main"><fmt:message
								key="mainpage" /> </a>
					</li>
					<li><a href="library?command=viewbooks"><fmt:message
								key="bookslist" /> </a>
					</li>
					<c:if test="${user.type eq 'USER'}">
						<li><a href="library?command=viewusersubscr"><fmt:message
									key="mysub" /> </a></li>
					</c:if>
				</c:if>
				<c:if test="${user.type eq 'ADMIN'}">
					<li><a href="library?command=viewpostedsubscrs"><fmt:message
								key="viewreq" /> </a>
					</li>
				</c:if>
			</ul>


		</div>

		<div id="content">

			<h3>
				<fmt:message key="bookreq" />
			</h3>
			<form name="subscrs" method="POST" action="library">
				<table border="2" cellpadding="2" cellspacing="2" width="400px">
					<c:if test='${reqs!="not found"}'>
						<tr>
							<th><fmt:message key="reqid" />
							</th>
							<th><fmt:message key="name" />
							</th>
							<th><fmt:message key="author" />
							</th>
						</tr>
						<c:forEach var="sub" items="${reqs.subs}">
							<c:forEach var="subBook" items="${sub.books}">
								<tr>
									<td><c:out value="${sub.id}">
										</c:out>
									</td>
									<td><c:out value="${subBook.name}">
										</c:out>
									</td>
									<td><c:out value="${subBook.author}">
										</c:out><input type="hidden" name="hb" value="${subBook.id}">
									</td>


								</tr>
							</c:forEach>
						</c:forEach>
					</c:if>
				</table>
				<hr />
				<h3>
					<fmt:message key="bookhave" />
				</h3>
				<table border="2" cellpadding="2" cellspacing="2" width="400px">
					<c:if test='${subscrs!="not found"}'>
						<tr>
							<th><fmt:message key="reqid" />
							</th>
							<th><fmt:message key="name" />
							</th>
							<th><fmt:message key="author" />
							</th>
						</tr>
						<c:forEach var="sub" items="${subscrs.subs}">
							<c:forEach var="subbook" items="${sub.books}">
								<tr>
									<td><c:out value="${sub.id}">
										</c:out>
									</td>
									<td><c:out value="${subbook.name}">
										</c:out>
									</td>
									<td><c:out value="${subbook.author}">
										</c:out>
									</td>
									<td><input type="checkbox" name="cb" value="${subbook.id}">
								</tr>
							</c:forEach>
						</c:forEach>
					</c:if>
				</table>
				<hr>
				<input type="hidden" name="command" value="returnbooks" /><a
					href="javascript:returnselected()"><fmt:message key="returnsel" />
				</a>
			</form>


		</div>
		<div id="close"></div>

	</div>


</body>
	</html>
</fmt:bundle>