<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="errorpage.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${userLocale}" />
<fmt:bundle basename="properties.text">
	<html>
<c:if test="${!user.logged}">
	<c:redirect url="index.jsp" />
</c:if>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css">
<title>Library</title>
</head>
<script language="JavaScript" type="text/javascript">
	function addbook() {
		document.bookForm.submit();
	}
</script>
<body>
	<div id=container>
		<div id="header"></div>

		<div id="user">
			<div id="lang">
				Language:
				<div id="langru">
					<a href="library?command=ref&lang=ru">русский</a>
				</div>
				<div id="langen">
					<a href="library?command=ref&lang=en">english</a>
				</div>
			</div>
			<c:if test="${!user.logged}">
				<fmt:message key="notlogged" />
			</c:if>
			<c:if test="${user.logged}">
				<fmt:message key="loggedas" />${user.name}, <c:if
					test="${user.type eq 'ADMIN'}">
					<fmt:message key="admin" />
				</c:if>
				<c:if test="${user.type eq 'USER'}">
					<fmt:message key="user" />
				</c:if>
				<a href="library?command=logout"><fmt:message key="logout" /> </a>
			</c:if>
		</div>
		<div id="navigation">

			<ul>
				<c:if test="${user.logged}">

					<li><a href="library?command=main"><fmt:message
								key="mainpage" /> </a></li>
					<li><a href="library?command=viewbooks"><fmt:message
								key="bookslist" /> </a></li>
					<c:if test="${user.type eq 'USER'}">
						<li><a href="library?command=viewusersubscr"><fmt:message
									key="mysub" /> </a></li>
					</c:if>
				</c:if>
				<c:if test="${user.type eq 'ADMIN'}">
					<li><a href="library?command=viewpostedsubscrs"><fmt:message
								key="viewreq" /> </a></li>
					<li><a href="javascript:addbook()"><fmt:message
								key="submit" /> </a></li>
				</c:if>
			</ul>


		</div>


		<div id="content">

			<form name="bookForm" method="POST" action="library">
				<fieldset>
					<legend>
						<fmt:message key="book" />
					</legend>
					<input type="hidden" name="command${varbook.id}"
						value="book${bookaction}" />
					<fmt:message key="name" />
					: <br /> <input type="text" name="name" value="${varbook.name}">
					<br />
					<fmt:message key="author" />
					: <br /> <input type="text" name="author"
						value="${varbook.author}"> <br />
					<fmt:message key="quantity" />
					: <br /> <input type="text" name="quantity"
						value="${varbook.quantity}">
				</fieldset>
			</form>

		</div>
		<div id="close"></div>

	</div>

</body>
	</html>
</fmt:bundle>