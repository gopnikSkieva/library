<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="errorpage.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${userLocale}" />
<fmt:bundle basename="properties.text">
	<html>
<c:if test="${!user.logged}">
	<c:redirect url="index.jsp" />
</c:if>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css">
<title>Library</title>
</head>
<script language="JavaScript" type="text/javascript">
	function removefromlist() {
		document.addtosubscr.submit();
	}
</script>
<body>
	<div id=container>
		<div id="header"></div>

		<div id="user">
			<div id="lang">
				Language:
				<div id="langru">
					<a href="library?command=ref&lang=ru">русский</a>
				</div>
				<div id="langen">
					<a href="library?command=ref&lang=en">english</a>
				</div>
			</div>
			<c:if test="${!user.logged}">
				<fmt:message key="notlogged" />
			</c:if>
			<c:if test="${user.logged}">
				<fmt:message key="loggedas" />${user.name}, <c:if
					test="${user.type eq 'ADMIN'}">
					<fmt:message key="admin" />
				</c:if>
				<c:if test="${user.type eq 'USER'}">
					<fmt:message key="user" />
				</c:if>
				<a href="library?command=logout"><fmt:message key="logout" /> </a>
			</c:if>
		</div>
		<div id="navigation">

			<ul>
				<c:if test="${user.logged}">

					<li><a href="library?command=main"><fmt:message
								key="mainpage" /> </a></li>
					<li><a href="library?command=viewbooks"><fmt:message
								key="bookslist" /> </a></li>
					<c:if test="${user.type eq 'USER'}">
						<li><a href="library?command=viewusersubscr"><fmt:message
									key="mysub" /> </a>
						</li>
					</c:if>
				</c:if>
				<c:if test="${user.type eq 'ADMIN'}">
					<li><a href="library?command=viewpostedsubscrs"><fmt:message
								key="viewreq" /> </a></li>
				</c:if>
			</ul>


		</div>

		<div id="content">

			<h3>
				<fmt:message key="welcomemsg" />
				, ${user.name}
			</h3>
			<form name="addtosubscr" method="POST" action="library">
				<table border="2" cellpadding="2" cellspacing="2" width="400px">
					<c:if test='${reqbooks!="not found"}'>
						<tr>
							<th><fmt:message key="bookid" /></th>
							<th><fmt:message key="name" /></th>
							<th><fmt:message key="author" /></th>
						</tr>
						<c:forEach var="book" items="${reqbooks.books}">
							<tr>
								<td><c:out value="${book.id}">
									</c:out></td>
								<td><c:out value="${book.name}">
									</c:out></td>
								<td><c:out value="${book.author}">
									</c:out></td>
								<td><c:out value="1">
									</c:out></td>
								<c:if test="${user.type eq 'USER'}">
									<td><input type="checkbox" name="cb" value="${book.id}">
										<input type="hidden" name="hb" value="${book.id}"></td>
								</c:if>
							</tr>
						</c:forEach>
					</c:if>
				</table>
				<hr>
				<input type="hidden" name="command" value="removefromlist" /> <a
					href="javascript:removefromlist()"><fmt:message
						key="bookremfromlst" /> </a> <a href="library?command=addsubscr"><fmt:message
						key="addsubscr" /> </a> <a href="library?command=addread"><fmt:message
						key="addread" /> </a>
			</form>



			<c:if test='${counterror=="true"}'>
				<h5 style="color: red;">
					<fmt:message key="gotbookerr" />
				</h5>
			</c:if>

		</div>
		<div id="close"></div>

	</div>

</body>
	</html>
</fmt:bundle>