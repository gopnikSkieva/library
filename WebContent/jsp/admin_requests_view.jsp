<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="errorpage.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${userLocale}" />
<fmt:bundle basename="properties.text">
	<html>
<c:if test="${!user.logged}">
	<c:redirect url="index.jsp" />ndex.jsp" />
</c:if>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css">
<title>Library</title>
</head>
<script language="JavaScript" type="text/javascript">
	function viewrequest(id) {
		this.location.href = "library?command" + id + "=viewrequest";
	}
</script>
<body>
	<div id=container>
		<div id="header"></div>

		<div id="user">
			<div id="lang">
				Language:
				<div id="langru">
					<a href="library?command=ref&lang=ru">русский</a>
				</div>
				<div id="langen">
					<a href="library?command=ref&lang=en">english</a>
				</div>
			</div>
			<c:if test="${!user.logged}">
				<fmt:message key="notlogged" />
			</c:if>
			<c:if test="${user.logged}">
				<fmt:message key="loggedas" />${user.name}, <c:if
					test="${user.type eq 'ADMIN'}">
					<fmt:message key="admin" />
				</c:if>
				<c:if test="${user.type eq 'USER'}">
					<fmt:message key="user" />
				</c:if>
				<a href="library?command=logout"><fmt:message key="logout" /> </a>
			</c:if>
		</div>
		<div id="navigation">

			<ul>
				<c:if test="${user.logged}">

					<li><a href="library?command=main"><fmt:message
								key="mainpage" /> </a></li>
					<li><a href="library?command=viewbooks"><fmt:message
								key="bookslist" /> </a> <c:if test="${user.type eq 'USER'}">
							<li><a href="library?command=viewusersubscr"><fmt:message
										key="mysub" /> </a>
							</li>
						</c:if>
				</c:if>
				<c:if test="${user.type eq 'ADMIN'}">
					<li><a href="library?command=viewpostedsubscrs"><fmt:message
								key="viewreq" /> </a></li>
				</c:if>
			</ul>


		</div>

		<div id="content">


			<form name="reqs" method="POST" action="library">
				<h3>
					<fmt:message key="bookreqa" />
				</h3>
				<table border="2" cellpadding="2" cellspacing="2" width="400px">
					<c:if test='${reqs!="not found"}'>
						<tr>
							<th><fmt:message key="reqid" />
							</th>
							<th><fmt:message key="useruc" />
							</th>
						</tr>
						<c:forEach var="sub" items="${reqs.subs}">
							<tr>
								<td><c:out value="${sub.id}">
									</c:out>
								</td>
								<td><c:out value="${sub.user.name}">
									</c:out>
								</td>
								<c:if test="${user.type eq 'ADMIN'}">
									<td><a href="javascript:viewrequest(${sub.id})"><fmt:message
												key="viewreq" /> </a>
									</td>
								</c:if>
							</tr>
						</c:forEach>
					</c:if>
				</table>

			</form>


		</div>
		<div id="close"></div>

	</div>

</body>
	</html>
</fmt:bundle>